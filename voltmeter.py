import time

import numpy as np
import dearpygui.dearpygui as dpg
dpg.create_context()


# creating data
x = []
y = []
for i in range(0, 500):
    x.append(i / 1000)
    y.append(0.5 + 0.5 * np.sin(50 * i / 1000))


data = []
cur_voltage_value = 5.45
time_period = 2


def update_series():
    cur_time = time.time()
    x = []
    y = []
    indexes = []
    for i in range(len(data)):
        if cur_time - data[i][0] < time_period:
            x.append(i / len(data))
            y.append(data[i][1])
        else:
            indexes.append(i)

    for index in reversed(indexes):
        data.pop(index)
    dpg.set_value("__mean_voltage", np.round(np.mean(y), 2))
    dpg.set_value("series_tag", [x, y])


def update_voltage():
    dpg.set_value("__current_voltage", cur_voltage_value)
# add a font registry


with dpg.font_registry():
    # default_font = dpg.add_font("NotoSerifCJKjp-Medium.otf", 20)
    ds_font = dpg.add_font("fonts/DS-DIGI.TTF", 20)


with dpg.window(label="OMG VOLTMETER"):
    # main table
    with dpg.table(header_row=False, resizable=True, policy=dpg.mvTable_SizingStretchProp,
                   borders_outerH=True, borders_innerV=True, borders_outerV=True):
        dpg.add_table_column()
        # first table element - table with params
        with dpg.table_row():

            with dpg.table(header_row=False, resizable=True, policy=dpg.mvTable_SizingStretchProp,
                           borders_outerH=True, borders_innerV=True, borders_outerV=True):

                for i in range(2):
                    dpg.add_table_column()

                with dpg.table_row():
                    l1 = dpg.add_text("Current voltage")
                    cur_voltage = dpg.add_text("4.5", indent=5, tag="__current_voltage", color=[0, 255, 0, 255])

                with dpg.table_row():
                    pass

                with dpg.table_row():
                    l2 = dpg.add_text("Input aggregation time")
                    input_text = dpg.add_input_text(default_value=2, indent=5, decimal=True, no_spaces=False, tag="__input_text")

                with dpg.table_row():
                    l3 = dpg.add_text("Average voltage")
                    mean_voltage = dpg.add_text("231.213", indent=5, tag="__mean_voltage", color=[0, 255, 0, 255])

                with dpg.table_row():
                    l4 = dpg.add_text("update time")
                    # update_button = dpg.add_button(label="update", indent=5, tag="__update_button", color=[0, 255, 0, 255],
                    #                                callback=update_voltage)
                    update_button = dpg.add_button(label="update", callback=update_voltage)

        # second table element - plot
        with dpg.table_row():

            with dpg.plot(label="Voltage plot", height=400, width=400):
                # optionally create legend
                dpg.add_plot_legend()

                # REQUIRED: create x and y axesя
                dpg.add_plot_axis(dpg.mvXAxis, label="x")
                dpg.add_plot_axis(dpg.mvYAxis, label="y", tag="y_axis")

                dpg.add_line_series(x, y, parent="y_axis", tag="series_tag")


# def toggle_layer2(sender):
#     show_value = dpg.get_value(sender)
#     dpg.configure_item("layer2", show=show_value)

with dpg.window(label="Analog_Voltmeter"):
    with dpg.drawlist(width=350, height=190):  # or you could use dpg.add_drawlist and set parents manually
        # padding 10
        with dpg.draw_layer():
            dpg.draw_circle((175, 190), 140)

            #5V
            dpg.draw_text((165, 27), "5V", color=(0, 255, 0), size=20)
            dpg.draw_line((175, 45), (175, 55))

            #0V
            dpg.draw_text((5, 170), "0V", color=(0, 255, 0), size=20)
            dpg.draw_line((30, 199), (40, 199))

            # 10V
            dpg.draw_text((319, 169), "10V", color=(0, 255, 0), size=20)
            dpg.draw_line((310, 199), (320, 199))

            for i in range(0, 181, 5):
                py = np.cos(np.deg2rad(i)) * 140
                px = np.sin(np.deg2rad(i)) * 140
                dpg.draw_circle((175-py, 190-px), 1)
                # dpg.draw_line()

        with dpg.draw_node(tag="main"):
            with dpg.draw_node(tag="arrow"):
                v = 5
                v *= 18
                px = np.cos(np.deg2rad(v)) * 135
                py = np.sin(np.deg2rad(v)) * 135
                a = dpg.draw_line((px, py), (0, 0), color=(0, 255, 0), thickness=2, tag="20")

                # dpg.draw_arrow((50, 120), (100, 115), color=(0, 200, 255), thickness=1, size=10)


    # dpg.show_style_editor()

    # dpg.bind_item_font(l1, default_font)
    # dpg.bind_item_font(l2, default_font)
    # dpg.bind_item_font(l3, default_font)

    dpg.bind_item_font(cur_voltage, ds_font)
    dpg.bind_item_font(mean_voltage, ds_font)
    dpg.bind_item_font(input_text, ds_font)

dpg.create_viewport(title='OMG VOLTMETER!!!', width=800, height=600)

dpg.setup_dearpygui()
dpg.show_viewport()
# dpg.start_dearpygui()
last_time_read = 0
reset_time = 0
while dpg.is_dearpygui_running():
    time_period = int(dpg.get_value("__input_text"))
    if time.time() - reset_time > time_period:
        reset_time = time.time()
        # print(len(data))
        for i in data:
            if time.time() - i[0] > time_period:
                data.remove(i)
        # print(len(data))

    if time.time() - last_time_read > 0.05:
        last_time_read = time.time()
        cur_voltage_value = np.round(np.random.normal(scale=0.2) + 5, 2)
        data.append((last_time_read, cur_voltage_value))
        # print(last_time_read)
        update_series()
        update_voltage()
        # dpg.apply_transform("arrow", )
        dpg.apply_transform("main", dpg.create_translation_matrix([174, 52+140]))
        dpg.apply_transform("arrow", dpg.create_rotation_matrix(np.deg2rad(-180 + (cur_voltage_value-5) * 18), [0, 0, 1]))

    dpg.render_dearpygui_frame()

dpg.destroy_context()
